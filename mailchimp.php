<?php

/*
Chatfuel MailChimp integration
Version 0.1 beta

First release by Fabio Grasso
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (http://creativecommons.org/licenses/by-sa/4.0/)

Documentation: https://bitbucket.org/fabiograsso82/chatfuel/wiki/Mailchimp

*/

require 'include/MailChimp.php';	// path for MailChimp API (https://github.com/drewm/mailchimp-api)

// VARIABLES:
$mailchimpListId = 'xxx'; // Mailchimp list id
$mailchimpAPIkey = 'xxx'; // Mailchimp API Key
$debug = 0;   // if true will write some useuful debug info on below log file
$debug_logfile = "mailchimp.log";
$block_ok = "Newsletter_ok";  // response block for 'OK' response when subscribed
$block_tryagain = "Newsletter_reg"; // response block for 'try again' in case of errors
$block_no = "Newsletter_no"; // response block when youser don't want to try again
$bot_icon = '🤖 '; // icon prefix for the response messages
$email_field = "EMAIL"; // field for email from Chatfuel JSON API - Warning: case sensitive!
$email = (!empty($_GET[$email_field]) ? preg_replace('/\s+/', '', trim($_GET[$email_field],".\t\n\r\0\x0B")) : ""); // get email address and remove spaces and extra dots
$msgGenericError="There was an error during newsletter registration:\n";
$msgTryAgain="\n\nDo you want to try again?";
$msgAlreadySubs="Email $email is already subscibed! Do you want register another address?";
$msgWrongEmail="Your email address ($email) seems to be invalid.";
$msgMissingField="A field is missing or invalid.";
$msgYes="Yes";
$msgNo="No";
$mailchimpStatus="pending"; // pending for confirmation email, subscribed for skip confirmation


$substitutionKey = array (  // put here substitution for the merge fields title. I.e. "first name" sent from Chatfuel became "FNAME" in Mailchimp,
  "first name" => "FNAME",  // "last name" became "LNAME" and so on. Insert an array value for each substitution in the form
  "last name" => "LNAME",   // "old name" => "new name"
  "gender" => "SEX",      // insert a comma for separate each substitution and remember to don't put the comma in the last one
  "email" => "EMAIL",
);
$substitutionValue = array (  // put here substitution for the merge fields content. I.e. for the "SEX" field I need "M" for male and "F" for female.
  "SEX" => array(             // the value in there MUST be the same of which you have in the mailchimp field configuration.
    "female" => "F",          // You need to declare first an array with the field name, then in this array you need to put another array with
    "male" => "M"             // "old value" => "new value"
  )
);



/* ---------------- here start the magic! ------------------ */ 


if($debug){
  error_log("\n\n------\n\nStart - " . date(DATE_RFC2822),3,$debug_logfile);
  error_log("\n\$substitutionKey value:\n",3,$debug_logfile);
  error_log(print_r($substitutionKey,1),3,$debug_logfile);
  error_log("\n\$substitutionValue value:\n",3,$debug_logfile);
  error_log(print_r($substitutionValue,1),3,$debug_logfile);
  error_log("\$_GET content: \n",3,$debug_logfile);
  error_log(print_r($_GET,1),3,$debug_logfile);
}

use \DrewM\MailChimp\MailChimp;  //Mailchimp Init

$mergeFields = $_GET; 
if(!empty($mergeFields[$email_field])) unset($mergeFields[$email_field]); // remove email address from merge fields list

foreach ($substitutionKey as $keyOld => $keyNew){ // change keyname based on substitution list
  if(!empty($mergeFields[$keyOld])){
     $mergeFields[$keyNew] = $mergeFields[$keyOld];
     unset($mergeFields[$keyOld]);
  }
}

foreach ($substitutionValue as $keyName => $keySubs){ //change key content base on substitution list
  if(!empty($mergeFields[$keyName])){
    foreach ($keySubs as $valueOld => $valueNew){
      $mergeFields[$keyName] = str_ireplace($valueOld, $valueNew, $mergeFields[$keyName]); 
    }
  }
}

if($debug) error_log("\n\$mergeFields content:\n",3,$debug_logfile);
if($debug) error_log(print_r($mergeFields,1),3,$debug_logfile);

if(!isset($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)){ // check if the email address is present and correct
  print_json($msgWrongEmail, 0);
  exit();
}

else { 
  $MailChimp = new MailChimp($mailchimpAPIkey);
  $result = $MailChimp->post("lists/$mailchimpListId/members", [
    'email_address' => $email,
    'merge_fields' => (object) $mergeFields,
    'status'        => $mailchimpStatus,
  ]);

  if ($MailChimp->success()) {
    print_json("", 1);
  }
  else {
    if (preg_match('/Member Exists/',$result['title'])){
      print_json($msgAlreadySubs, 0);
    }
    elseif (preg_match('/Invalid Resource/',$result['title']) && (preg_match('/looks fake or invalid/',$result['detail']))){
      print_json($msgWrongEmail . $msgTryAgain, 0);
    }
    elseif (preg_match('/Invalid Resource/',$result['title']) && (preg_match('/merge fields were invalid/',$result['detail']))){
      print_json($msgMissingField . $msgTryAgain, 0);
    }
    else{
      print_json($msgGenericError . $MailChimp->getLastError() . $msgTryAgain, 0);
    }
  }

  if($debug){
    error_log("\n\nMailChimp Last Response:\n",3,$debug_logfile);
    error_log(print_r($MailChimp->getLastResponse(),1),3,$debug_logfile);
    error_log("\n\nMailChimp Last Request:\n",3,$debug_logfile);
    error_log(print_r($MailChimp->getLastRequest(),1),3,$debug_logfile);
  }
  exit();
}



function print_json($message, $type = 0 ){
  global $block_tryagain;
  global $block_ok;
  global $block_no;
  global $bot_icon;
  global $msgNo;
  global $msgYes;
  global $debug;
  global $debug_logfile;

  if($type == 1){
    $myObj = [
      "redirect_to_blocks" => [$block_ok]
    ];
  }

  else {
    $myObj = [
      "messages" => [
        [
          "attachment" => [
            "type" => "template",
            "payload" => [
              "template_type" => "button",
              "text" => "$bot_icon $message",
              "buttons" => [
                [
                  "type" => "show_block",
                  "block_names" => [ $block_tryagain ],
                  "title" => $msgYes
                ],
                [
                  "type" => "show_block",
                  "block_names" => [ $block_no ],
                  "title" => $msgNo
                ]
              ]
            ]
          ]
        ]
      ]
    ];
  }
 
  echo json_encode($myObj, JSON_PRETTY_PRINT);
  if($debug){
    error_log("\n\n\n------------\n\n\nJson content:\n\n",3,$debug_logfile);
    error_log(json_encode($myObj, JSON_PRETTY_PRINT),3,$debug_logfile);
  }

}

?>