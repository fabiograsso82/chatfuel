README
======

This repository contains some of my integrations between Chatfuel and some other tools, like Mailchimp and MySQL database.

It's a "work in progress", so I will add new stuff in future. I create this scripts for my own use, but can be useful for other people working with Chatfuel.

I also use this as an exercise to better exploit the JSON response to Chatfuel ;-)

This is the results of my work. You can take as starting point for your implementation.

Documentation
-------------
You can find the documentation for the scripts on the [**Wiki**](https://bitbucket.org/fabiograsso82/chatfuel/wiki) section



Disclaimer
----------

* I state that I am not a programmer, my code will certainly be ugly and poorly optimized. but ... it works, it does what it takes for me and can be reused for someone who has similar needs.
* This software is delivered "as is" and should be considered as experimental, unstable and take it just as an example of which kind of integration can be with Chatfuel
* I repeat again ... This software is delivered "as is" and should be considered as experimental and unstable. Test it before using in production environment

What is this repository for?
----------------------------

* Chatfuel Integrations with Mailchimp and MySQL

Contribution guidelines
-------------

* Any kind of contribution is welcome. I don't have much time to devote to this project but feel free to contribute as you wish or create new forks.

Info & License
-------------

* First release by [Fabio Grasso](https://bitbucket.org/fabiograsso82/)
* This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) 

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
