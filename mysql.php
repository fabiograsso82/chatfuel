<?php

/*
Chatfuel MySQL integration
Version 0.1 beta

First release by Fabio Grasso
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (http://creativecommons.org/licenses/by-sa/4.0/)

Documentation: https://bitbucket.org/fabiograsso82/chatfuel/wiki/MySQL

*/

$mysql_hostname = "localhost";
$mysql_port = 3306;
$mysql_dbname = "bot";
$mysql_user = "bot";
$mysql_password = "password";
$mysql_table = "user_info";
$mysql_charset = "utf8";

$userIdAttribute = "messenger_user_id";


$debug = false; 
$debug_logfile = "mysql.log"; 

if($debug) error_log("\n\n------\n\nStart - " . date(DATE_RFC2822),3,$debug_logfile);


if(count($_POST) > 0){  // check if there is some data in post or get
    $params = $_POST;
    if($debug) error_log("Data taken from POST\n",3,$debug_logfile);
} elseif(count($_GET) > 0){
    $params = $_GET;
    if($debug) error_log("Data taken from GET\n",3,$debug_logfile);
} else {
    echo "Error... Nothing in GET or POST";
    exit(1);
}
$params["timestamp_lastchange"] = date('Y-m-d H:i:s');  // generate last change timestamp

// connecting to mysql server
$mysqli = new mysqli($mysql_hostname, $mysql_user, $mysql_password, $mysql_dbname, $mysql_port);
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    exit(1);
}
$mysqli->set_charset($mysql_charset);


if($debug) error_log("Connection: " . $mysqli->host_info . "\n\n",3,$debug_logfile);
if($debug) error_log("Params: ",3,$debug_logfile);
if($debug) error_log(print_r($params,1),3,$debug_logfile);
 
if( $columns = $mysqli->query("SHOW columns FROM `$mysql_table`;") ) {    // get columns list for table
    while($r=$columns->fetch_assoc()) {
        $tableColumns[] = $r["Field"];  // put all columns name in an array
    }
    $columns->free();
    foreach($params as $key => $value){   // process all the _POST parameters
        if(!in_array($key, $tableColumns)){    // check if the _POST key have already a columns in table
            if($debug) error_log("Key $key doesn't exist, adding...\n",3,$debug_logfile);
            $mysqli->query("ALTER TABLE `$mysql_table` ADD `$key` VARCHAR(255)");   // if not, create it
        }
        if($debug) error_log("Key: '$key' - Value: '$value'\n",3,$debug_logfile);
    }

    if ( $id = $mysqli->query("SELECT `$userIdAttribute` FROM `$mysql_table` WHERE `$userIdAttribute`= " . $params[$userIdAttribute]) ){
        if($debug) error_log("User exist in table? " . $id->num_rows . "\n",3,$debug_logfile); 
        if($id->num_rows > 0) { // check if user is already in table
            $finalQuery = "UPDATE `$mysql_table` SET ";
            foreach($params as $key => $value){
                if($key != $userIdAttribute) $finalQuery .= "`$key` = \"$value\",";  // prepare SQL query for each value, escluding messenger_user_id
            }
            $finalQuery = substr($finalQuery, 0, -1);  // remove the last comma
            $finalQuery .= " WHERE `$userIdAttribute`=" . $params[$userIdAttribute] . ";";
        } else {
            $finalQuery = "INSERT INTO $mysql_table (`" . implode(array_keys($params), "`,`") . "`) VALUES (\"" . implode(array_values($params), "\",\"") . "\");";  // prepare SQL query for insert a new entry
        }

        if($debug) error_log("Result:\n".$finalQuery."\n",3,$debug_logfile);

    } else {
        echo "ERROR IN SQL QUERY FOR ID";
        $mysqli->close();
        exit(1);
    }

} else {
    print "ERROR IN SQL QUERY FOR COLUMNS";
    $mysqli->close();
    exit(1);
}

if ( $result = $mysqli->query($finalQuery)){
    if($debug){
        // put here some debug stuff
    }
} else {
    print "ERROR IN FINAL SQL QUERY";
}

$mysqli->close();

// print a JSON output, just for not generate an error on Chatfuel side and for insert an attribut in user that confirm the SQL registration
echo <<<EOF
{ 
    "set_attributes": 
      {
        "z_saved_mysql": "true"
      }
}
EOF;

?>