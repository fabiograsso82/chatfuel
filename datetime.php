<?php

/*
Chatfuel Date/Time
Version 0.1 beta

First release by Fabio Grasso
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (http://creativecommons.org/licenses/by-sa/4.0/)

Documentation: https://bitbucket.org/fabiograsso82/chatfuel/wiki/Get%20Date%20and%20Time

*/

$typeAttribute = "type";
$type = 1;  // types: 1 = date only, 2 = date and time, 3 = time only

$dateFormatAttribute = "dateformat";
$dateFormat = "Y-m-d";
$dateRespAttribute = "dateattribute";
$dateResp = "date";

$timeFormatAttribute = "timeformat";
$timeFormat = "H:i:s";
$timeRespAttribute = "timeattribute";
$timeResp = "time";

if(!empty($_POST[$dateFormatAttribute])){  // check if there is some data in post or get
    $dateFormat = $_POST[$dateFormatAttribute];
} elseif(!empty($_GET[$dateFormatAttribute])){
    $dateFormat = $_GET[$dateFormatAttribute];
}

if(!empty($_POST[$dateRespAttribute])){  // check if there is some data in post or get
    $dateResp = $_POST[$dateRespAttribute];
} elseif(!empty($_GET[$dateRespAttribute])){
    $dateResp = $_GET[$dateRespAttribute];
}

if(!empty($_POST[$timeFormatAttribute])){  // check if there is some data in post or get
    $timeFormat = $_POST[$timeFormatAttribute];
} elseif(!empty($_GET[$timeFormatAttribute])){
    $timeFormat = $_GET[$timeFormatAttribute];
}

if(!empty($_POST[$timeRespAttribute])){  // check if there is some data in post or get
    $timeResp = $_POST[$timeRespAttribute];
} elseif(!empty($_GET[$timeRespAttribute])){
    $timeResp = $_GET[$timeRespAttribute];
}

if(!empty($_POST[$typeAttribute])){  // check if there is some data in post or get
    $type = $_POST[$typeAttribute];
} elseif(!empty($_GET[$typeAttribute])){
    $type = $_GET[$typeAttribute];
}

$output["set_attributes"]=array(); //initialize output array

if($type == 1 || $type == 2) $output["set_attributes"] += [$dateResp => date($dateFormat)];
if($type == 2 || $type == 3) $output["set_attributes"] += [$timeResp => date($timeFormat)];

echo json_encode($output, JSON_PRETTY_PRINT);
?>